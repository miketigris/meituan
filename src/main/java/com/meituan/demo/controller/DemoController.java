package com.meituan.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mike on 2018/5/21.
 */
@RestController
@Api
public class DemoController {

    @PostMapping
    @ApiOperation(value="获取书本",notes="Post接口")
    public String getBooks(){

        return "《Spring Boot》";

    }

}
